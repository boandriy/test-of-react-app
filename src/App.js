import React, {useState} from 'react';
import Navigation from "./components/Navigation";
import './App.css';

function App() {

  const [input, setInput] = useState('test');

  const handleInputChange = (e) => {
    setInput(e.target.value);
  };

  const eraseText = () => {
      setInput('');
  };

  return (
    <div>
        <input value={input} onChange={handleInputChange} type="text"/>
        <Navigation erase={eraseText} title={input} />
    </div>
  );
}


export default App;
