import React from "react";

function Navigation(props) {
    return (
        <div>
            <p>{props.title}</p>
            <button onClick={props.erase}>
                Erase the text
            </button>
            {props.title.length > 3 && <p>the title is pretty long :)</p>}
        </div>
    )
}


export default Navigation;
